package com.example.demo.com.compment;

import com.example.demo.com.inter.CompactDisc;
import org.springframework.stereotype.Component;

import javax.inject.Named;

//@Component("new")
@Named("new")
public class NewSgtPeppers implements CompactDisc {
    @Override
    public void play() {
        System.out.println("play New");
    }
}
