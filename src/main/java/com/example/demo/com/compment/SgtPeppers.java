package com.example.demo.com.compment;

import com.example.demo.com.inter.CompactDisc;
import org.springframework.stereotype.Component;

import javax.inject.Named;


//@Component("sgtPeppers")
@Named("sgtPeppers")
public class SgtPeppers implements CompactDisc {
    private String title = "hello333333 CD";

    @Override
    public void play() {
        System.out.println(title);
    }
}
