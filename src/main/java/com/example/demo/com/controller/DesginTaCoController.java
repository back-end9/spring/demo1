package com.example.demo.com.controller;

import com.example.demo.com.model.Ingredient;
import com.example.demo.com.model.Taco;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

//import javax.validation.Valid;
import javax.validation.Valid;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@Controller
@RequestMapping("/design")
public class DesginTaCoController {

    @GetMapping()
    public String showDesignForm(Model model){
        log.error("熄火了");
        List<Ingredient> ingredients = Arrays.asList(
            new Ingredient("FLTO","Flour Tortilla", Ingredient.Type.WRAP),
            new Ingredient("COTO","Corn Torilla", Ingredient.Type.WRAP),
            new Ingredient("GRBF", "Ground Beef", Ingredient.Type.PROTEIN)
        );

        Ingredient.Type[] types = Ingredient.Type.values();
        for(Ingredient.Type type : types) {
            model.addAttribute(type.toString().toLowerCase(),filtterByType(ingredients,type));
        }

        model.addAttribute("desgin",new Taco());

        return "design";
    }

    private List<Ingredient> filtterByType(List<Ingredient> ingredients, Ingredient.Type type){

        return ingredients.stream()
                .filter(x -> x.getType().equals(type))
                .collect(Collectors.toList());
    }

    @PostMapping
    public String processDesign(@Valid  Taco design, Errors errors){
        if(errors.hasErrors()){
            return "design";
        }
        log.info("Processing design:");
        log.info(design.toString());
        return "redirect:/orders/current";
    }
}
