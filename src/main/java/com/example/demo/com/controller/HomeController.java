package com.example.demo.com.controller;


import com.example.demo.com.inter.CompactDisc;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

import javax.inject.Inject;
import javax.inject.Inject;


@Controller
public class HomeController {

    //@Autowired
    @Qualifier("new")
    @Inject()
    private CompactDisc cd;

    @GetMapping("/")
    public String home(){
        cd.play();
        return "home";
    }


}
