package com.example.demo.com.model;


import lombok.Data;
import org.hibernate.validator.constraints.CreditCardNumber;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.Digits;
import javax.validation.constraints.Pattern;

@Data
public class Order {

    @NotBlank(message = "订单名称必须填写")
    private String name;
    private String street;
    private String city;
    private String state;
    private String zip;

//    @CreditCardNumber(message = "不是一个有效的信用卡")
    private String ccNumber;

    @Pattern(regexp = "^(0[1-9]|1[0-2])([\\/])([1-9][0-9])$",message = "格式必须为MM/YY")
    private String ccExpiration;

    @Digits(integer = 3,fraction = 0,message = "不是有效CVV")
    private String ccCVV;
}
