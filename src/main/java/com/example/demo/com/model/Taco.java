package com.example.demo.com.model;


import lombok.Data;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.List;

@Data
public class Taco {

    @NotNull
    @Size(min=5,message="name必须填写")
    private String name;


    @Size(min=1,message = "你必须选择一个checkbox")
    private List<String> ingredients;
}
